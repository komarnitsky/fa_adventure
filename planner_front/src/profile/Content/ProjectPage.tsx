import React, {FC, useEffect, useState} from 'react';
import {ProjectProps} from './types';
import {TaskProps} from '../Task/types';
import {TaskListContainer} from '../Task/TaskListContainer';
import {TaskDetailContainer} from '../Task/TaskDetailContainer';
import {getTaskList} from '../../Client';


export const ProjectPage: FC<ProjectProps> = ({project}) => {
  const [data, setData] = useState<TaskProps[]>([]);
  const [active, setActive] = useState<TaskProps | undefined>(undefined);

  const updateTaskList = () => {
    getTaskList(project).then((result) => {
      // @ts-ignore
      setData(result.data.tasks);
    });
  };

  useEffect(() => {
    updateTaskList();
  }, [project]);

  return (
    <div className="h-100 flex-row" style={{alignItems: 'flex-start'}}>
      <TaskListContainer {...{data, setActive, updateTaskList}} />
      <hr className="h-100"/>
      <TaskDetailContainer {...{active, setActive, updateTaskList}} />
    </div>
  );
};
