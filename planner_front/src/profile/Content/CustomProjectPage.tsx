import {useParams} from 'react-router-dom';
import React from 'react';
import {ProfilePage} from '../profilePage';

export const CustomProjectPage = () => {
  const {project} = useParams<{project: string}>();
  return <ProfilePage activeTab={project} />;
};
