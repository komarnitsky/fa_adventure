import React from 'react';
import {Card, Typography} from 'antd';
import {ChartContainer} from '../../analytics/ChartContainer/ChartContainer';
import './Content.css';

const {Title} = Typography;

export const AnalyticsPage = () => (
  <Card className="pageContainer">
    <Title>Аналитика</Title>
    <ChartContainer chartType="heatmap" />
  </Card>
);
