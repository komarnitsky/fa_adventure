import React, {FC} from 'react';
import {Button, Layout, Typography} from 'antd';
import './PageHeader.css';
import {HeaderProps} from './types';

const {Header} = Layout;
const {Title} = Typography;

export const PageHeader: FC<HeaderProps> = ({username}) => {
  return (
    <Header className="flex-row">
      <Title
        level={4}
        style={{
          color: 'white', justifySelf: 'left', flex: 1, margin: 0,
        }}
      >
      Личный кабинет
        {' '}
        {username}
      </Title>
      <Button
        ghost
        style={{justifySelf: 'right'}}
        onClick={() => {
          localStorage.removeItem('token');
          window.open('/', '_self');
        }}
      >
      Выход
      </Button>
    </Header>
  );
};
