import React, {FC} from 'react';
import {Layout} from 'antd';
import 'antd/dist/antd.css';
import {PageSider} from './Sider/PageSider';
import {PageHeader} from './Header/PageHeader';
import {ProfileProps} from './types';
import {AnalyticsPage} from './Content/AnalyticsPage';
import {ProjectPage} from './Content/ProjectPage';

const {Content} = Layout;

export const ProfilePage: FC<ProfileProps> = ({activeTab = 'analytics'}) => (
  <Layout style={{minHeight: '100vh'}}>
    <PageSider collapsed={false} activeTab={activeTab} />
    <Layout>
      <PageHeader username={localStorage.getItem('username') || ''} />
      <Content>
        {activeTab === 'analytics' ?
          <AnalyticsPage /> :
          <ProjectPage project={activeTab} />}
      </Content>
    </Layout>
  </Layout>
);
