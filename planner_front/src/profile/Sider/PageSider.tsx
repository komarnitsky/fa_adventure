import React, {FC, useEffect, useState} from 'react';
import {
  Input, Layout, Menu, Modal,
} from 'antd';
import './PageSider.css';
import {Link} from 'react-router-dom';
import {
  PieChartOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import {SiderProps} from './types';
import {projectIcons} from '../../constants';
import * as Client from '../../Client';
import {createNewProject} from '../../Client';

const {Sider} = Layout;

export const PageSider: FC<SiderProps> = ({collapsed = false, activeTab}) => {
  const [isCollapsed, setCollapsed] = useState(collapsed);
  const [modalVisible, setModalVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState('');

  const [projectsList, setProjectsList] = useState([]);

  const onSiderCollapse = () => {
    setCollapsed(!isCollapsed);
  };

  useEffect(() => {
    Client.getProjectsList().then(((result) => {
      const {data} = result;
      // @ts-ignore
      setProjectsList(data.map(({name, uuid}) => {
        return (
          // @ts-ignore
          <Menu.Item key={uuid} icon={projectIcons[name]}>
            <Link to={`/${uuid}`}>{name}</Link>
          </Menu.Item>
        );
      }));
    }));
  }, [modalVisible]);

  return (
    <Sider collapsible collapsed={isCollapsed} onCollapse={onSiderCollapse}>
      <div className="logo" />
      <Menu theme="dark" defaultSelectedKeys={[activeTab]} mode="inline">
        <Menu.Item key="analytics" icon={<PieChartOutlined />}>
          <Link to="/analytics">Аналитика</Link>
        </Menu.Item>
        {projectsList}
        <Menu.Item key="createNewProject" icon={<PlusOutlined />}>
          <a onClick={() => setModalVisible(true)}>Новый проект</a>
        </Menu.Item>
      </Menu>
      <Modal
        title="Создать проект"
        visible={modalVisible}
        onOk={() => createNewProject(setConfirmLoading, modalText, setModalVisible, setModalText)}
        onCancel={() => setModalVisible(false)}
        confirmLoading={confirmLoading}
      >
        <Input
          placeholder="Имя нового проекта"
          onChange={(e) => setModalText(e.target.value)}
        />
      </Modal>
    </Sider>
  );
};
