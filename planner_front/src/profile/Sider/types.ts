export interface SiderProps {
  collapsed?: boolean;
  activeTab: string;
}
