import React, {FC, useState} from 'react';
import {TasksListProps} from './types';
import {TasksList} from './TasksList';
import {TaskListHeader} from './TaskListHeader';

export const TaskListContainer: FC<TasksListProps> = ({data, setActive}) => {
  const [groupby, setGroupby] = useState('progress');
  const [search, setSearch] = useState('');

  return (
    <div className="w-100 flex-col">
      <TaskListHeader
        groupby={groupby}
        setGroupby={setGroupby}
        search={search}
        setSearch={setSearch}
      />
      <hr style={{width: '99%'}}/>
      <TasksList
        data={data}
        groupByKey={groupby}
        search={search}
        setActive={setActive}
      />
    </div>
  );
};
