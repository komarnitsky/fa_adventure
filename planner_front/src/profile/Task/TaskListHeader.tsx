import React, {FC, useState} from 'react';
import {
  Button, DatePicker, Form, Input, Modal, Select, Typography,
} from 'antd';
import {PlusOutlined} from '@ant-design/icons';
import {GroupbyProps, SearchProps, TaskListHeaderProps} from './types';
import {createTask} from '../../Client';
import {PrioritySelect} from './TaskDetailContainer';
import {Moment} from 'moment';

const GroupBySelect: FC<GroupbyProps> = ({groupby, setGroupby}) => (
  <Select
    defaultValue={groupby}
    style={{minWidth: '150px'}}
    onSelect={(e) => setGroupby(e)}
  >
    <Select.Option value="status">статусу задачи</Select.Option>
    <Select.Option value="deadline">дедлайну</Select.Option>
    <Select.Option value="priority">приоритету</Select.Option>
    <Select.Option value="none">---</Select.Option>
  </Select>
);

const SearchField: FC<SearchProps> = ({search, setSearch}) => (
  <Input onChange={(e) => setSearch(e.target.value)} />
);

export const TaskListHeader: FC<TaskListHeaderProps> = ({
  groupby,
  setGroupby,
  search,
  setSearch,
}) => {
  const [modal, setModal] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  const [newTitle, setNewTitle] = useState('');
  const [newPriority, setNewPriority] = useState<'low' | 'high' | 'critical'>('low');
  const [newDeadline, setNewDeadline] = useState<Moment>();
  const [newDesc, setNewDesc] = useState('');
  const [newSkillPoints, setNewSkillPoints] = useState(1);

  return (
    <div className="flex-row list-header space-between">
      <div className="flex-row header-section">
        <Typography.Text>Группировать по:</Typography.Text>
        <GroupBySelect groupby={groupby} setGroupby={setGroupby} />
      </div>
      <div className="flex-row header-section">
        <Typography.Text>Поиск:</Typography.Text>
        <SearchField search={search} setSearch={setSearch} />
      </div>
      <Button onClick={() => setModal(true)}>
        <PlusOutlined />
      Создать задачу
      </Button>
      <Modal
        title="Создание задачи"
        centered
        visible={modal}
        onCancel={() => {
          setModal(false);
          setConfirmLoading(false);
        }}
        confirmLoading={confirmLoading}
        onOk={() => {
          setConfirmLoading(true);
          createTask(newTitle, newPriority, newDesc, newSkillPoints, newDeadline)
              .then(() => {
                setConfirmLoading(false);
                window.location.reload();
              })
              .catch(() => setConfirmLoading(false));
          setModal(false);
        }}
      >
        <Form
          layout="vertical">
          <Form.Item label="Краткое описание" rules={[{required: true}]} required>
            <Input onChange={(e) => setNewTitle(e.target.value)} />
          </Form.Item>
          <Form.Item label="Приоритет" required>
            {/* @ts-ignore */}
            <PrioritySelect current={newPriority} onSelect={(e) => setNewPriority(e)} />
          </Form.Item>
          <Form.Item label="Дедлайн" required rules={[{required: true}]}>
            {/* @ts-ignore */}
            <DatePicker onSelect={setNewDeadline}/>
          </Form.Item>
          <Form.Item label="SkillPoints" required rules={[{required: true}]}>
            <Input
              type="number"
              onChange={(e) => setNewSkillPoints(parseInt(e.target.value))}
              defaultValue={1}
              min={1}
              max={10}
            />
          </Form.Item>
          <Form.Item label="Расширенное описание">
            {/* @ts-ignore */}
            <Input.TextArea rows={4} onChange={((e) => setNewDesc(e.target.value))} />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};
