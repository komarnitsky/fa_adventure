import React, {FC} from 'react';
import {Typography} from 'antd';
// @ts-ignore
import lowPriority from '../../res/medium.svg';
// @ts-ignore
import highPriority from '../../res/high.svg';
// @ts-ignore
import criticalPriority from '../../res/highest.svg';
import {TaskProps} from './types';
import {Step} from './Step';
import {progresses} from '../../constants';

export const TaskComponent: FC<TaskProps & {
  setActive: (value: TaskProps) => void
}> = ({
  uuid,
  priority,
  name,
  desc,
  status,
  deadline,
  setActive,
  // eslint-disable-next-line camelcase
  created_at,
  // eslint-disable-next-line camelcase
  skill_points,
  project,
}) => {
  let priorityImage;
  switch (priority) {
    case 'critical':
      priorityImage = criticalPriority;
      break;
    case 'high':
      priorityImage = highPriority;
      break;
    case 'low':
      priorityImage = lowPriority;
      break;
    default:
      priorityImage = highPriority;
  }

  const onClick = () => {
    const active = {
      uuid,
      priority,
      name,
      desc,
      status,
      deadline,
      created_at,
      skill_points,
      project,
    };
    setActive(active);
  };

  return (
    <div
      className="flex-row task-container clickable"
      onClick={() => onClick()}
    >
      <img src={priorityImage} className="priority-icon" />
      <Typography.Text className="task-title-sm">{name}</Typography.Text>
      <Step progress={progresses.indexOf(status)} />
    </div>
  );
};
