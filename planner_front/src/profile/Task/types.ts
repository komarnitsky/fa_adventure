import {Moment} from 'moment';

export interface TaskProps {
  uuid: string;
  name: string;
  desc?: string;
  // eslint-disable-next-line camelcase
  created_at: Moment;
  deadline: Moment;
  priority: 'high' | 'low' | 'critical';
  // eslint-disable-next-line camelcase
  skill_points: number;
  status: string;
  project: string;
}

export interface TasksListProps {
  data: TaskProps[];
  search?: string;
  groupByKey?: string;
  setActive?: (value: TaskProps) => void;
}

export interface TaskDetailProps {
  active?: TaskProps;
  setActive: (value?: TaskProps) => void;
}

export interface SearchProps {
  search: string;
  setSearch: (value: string) => void;
}

export interface GroupbyProps {
  groupby: string;
  setGroupby: (value: string) => void;
}

export type TaskListHeaderProps = GroupbyProps & SearchProps;
