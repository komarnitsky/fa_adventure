import React, {FC} from 'react';
import {Space} from 'antd';
import {stepIcons, steps} from '../../constants';

export const Step: FC<{progress: number, style?: any}> = ({progress, style}) => {
  return (
    <Space className="flex-row" style={{width: '150px', ...style}}>
      {stepIcons[progress]}
      {steps[progress]}
    </Space>
  );
};
