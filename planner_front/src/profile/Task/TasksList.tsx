import React, {FC, useEffect, useState} from 'react';
import {Divider} from 'antd';
import moment from 'moment';
import {TaskProps, TasksListProps} from './types';
import {TaskComponent} from './TaskComponent';
import {priorityHumane, progresses, steps} from '../../constants';

export const TasksList: FC<TasksListProps> = ({
  data,
  groupByKey,
  search,
  setActive,
}) => {
  const [dataMask, setDataMask] = useState({});

  const groupBy = (values: TaskProps[], key: string) => values.reduce((rv: any, x: TaskProps) => {
    // @ts-ignore
    rv[x[key]] = rv[x[key]] || [];
    // @ts-ignore
    rv[x[key]].push(x);
    return rv;
  }, {});

  useEffect(() => {
    let myData = data;
    if (search) {
      myData = myData.filter(
          (e) => (e.name + e.desc).toLowerCase().includes(search.toLowerCase()),
      );
    }

    console.log(groupByKey);
    if (!groupByKey || groupByKey === 'none') {
      setDataMask({'Текущие задачи': myData});
    } else {
      setDataMask(groupBy(myData, groupByKey));
    }
  }, [groupByKey, search, data]);

  const renderSection = (
      sectionTitle: any,
      sectionValues: TaskProps[],
  ) => {
    let formattedTitle;
    if (groupByKey === 'deadline') {
      formattedTitle = moment(sectionTitle).format('DD-MM-YYYY');
    } else if (groupByKey === 'progress') {
      formattedTitle = steps[progresses.indexOf(sectionTitle)];
    } else if (groupByKey === 'priority') {
      // @ts-ignore
      formattedTitle = priorityHumane[sectionTitle];
    } else {
      formattedTitle = sectionTitle;
    }
    return (
      <div className="flex-col" key={sectionTitle}>
        <Divider orientation="left">{formattedTitle}</Divider>
        {sectionValues.map((e: TaskProps) => (
          <TaskComponent {...e} key={e.name} setActive={setActive!} />
        ))}
      </div>
    );
  };

  return (
    <div className="h-100">
      {/* @ts-ignore */}
      {Object.keys(dataMask).map((e) => renderSection(e, dataMask[e]))}
    </div>
  );
};
