import React, {
  Dispatch, FC, SetStateAction, useEffect, useState,
} from 'react';
import {CloseOutlined} from '@ant-design/icons';
import {DatePicker, Select, Typography} from 'antd';
import moment, {Moment} from 'moment';
import {TaskDetailProps} from './types';
// @ts-ignore
import lowPriority from '../../res/medium.svg';
// @ts-ignore
import highPriority from '../../res/high.svg';
// @ts-ignore
import criticalPriority from '../../res/highest.svg';
import {progresses, steps} from '../../constants';
import {Step} from './Step';
import {updateTask} from '../../Client';

export const PrioritySelect: FC<{
  current: string;
  onSelect: Dispatch<SetStateAction<'high' | 'low' | 'critical'>>
}> = ({current, onSelect}) => (
  // @ts-ignore
  <Select value={current} style={{width: '180px'}} onSelect={onSelect}>
    <Select.Option value="low">
      <img src={lowPriority} className="priority-icon" alt="Низкий" />
      Низкий
    </Select.Option>
    <Select.Option value="high">
      <img src={highPriority} className="priority-icon" alt="Высокий" />
      Высокий
    </Select.Option>
    <Select.Option value="critical">
      <img
        src={criticalPriority}
        className="priority-icon"
        alt="Немедленный"
      />
      Немедленный
    </Select.Option>
  </Select>
);

const StatusSelect: FC<{
  current: number,
  onSelect: Dispatch<SetStateAction<number>>
}> = ({current, onSelect}) => (
  <Select value={current} style={{width: '180px'}} onSelect={onSelect}>
    {steps.map((e: string, index: number) => (
      <Select.Option value={index} key={e}>
        <Step progress={index} style={{width: 120}}/>
      </Select.Option>
    ))}
  </Select>
);

const StatusTable: FC<{
  title: string;
  setTitle: Dispatch<SetStateAction<string>>;
  progress: number;
  setProgress: Dispatch<SetStateAction<number>>;
  priority: string;
  setPriority: Dispatch<SetStateAction<'high' | 'low' | 'critical'>>;
  desc?: string;
  setDesc: Dispatch<SetStateAction<string | undefined>>;
  deadline: Moment;
  setDeadline: Dispatch<SetStateAction<Moment>>;
}> = ({
  title,
  setTitle,
  progress,
  setProgress,
  priority,
  setPriority,
  desc,
  setDesc,
  deadline,
  setDeadline,
}) => (
  <table className="status-table">
    <tbody>
      <tr>
        <td><Typography.Text>Приоритет:</Typography.Text></td>
        <td><PrioritySelect current={priority} onSelect={setPriority} /></td>
      </tr>
      <tr>
        <td><Typography.Text>Статус:</Typography.Text></td>
        <td><StatusSelect current={progress} onSelect={setProgress} /></td>
      </tr>
      <tr>
        <td><Typography.Text>Дедлайн:</Typography.Text></td>
        <td><DatePicker value={moment(deadline)} onSelect={setDeadline} style={{width: '180px'}}/></td>
      </tr>
    </tbody>
  </table>
);

export const TaskDetailContainer: FC<TaskDetailProps> = ({
  active,
  setActive,
}) => {
  if (!active) {
    return null;
  }

  const [title, setTitle] = useState(active.name);
  const [progress, setProgress] = useState(progresses.indexOf(active.status));
  const [priority, setPriority] = useState(active.priority);
  const [desc, setDesc] = useState(active.desc);
  const [deadline, setDeadline] = useState(active.deadline);

  // Task changed
  useEffect(() => {
    setTitle(active.name);
    setProgress(progresses.indexOf(active.status));
    setPriority(active.priority);
    setDesc(active.desc);
    setDeadline(active.deadline);
  }, [active]);

  // Update current task
  useEffect(() => {
    if (
      title !== active.name ||
      progress !== progresses.indexOf(active.status) ||
      priority !== active.priority ||
      desc !== active.desc ||
      deadline !== active.deadline
    ) {
      updateTask(active.uuid, title, progress, priority, desc, deadline);
    }
  }, [title, progress, priority, desc, deadline]);

  return (
    <div className="detail-container">
      <div className="flex-col">
        <div
          className="flex-row"
          style={{
            justifyContent: 'right',
            alignSelf: 'top',
          }}
        >
          <CloseOutlined onClick={() => setActive(undefined)} />
        </div>
        <Typography.Title
          level={4}
          editable={{onChange: setTitle}}
        >
          {title}
        </Typography.Title>
        <StatusTable {...{
          title,
          setTitle,
          progress,
          setProgress,
          priority,
          setPriority,
          desc,
          setDesc,
          deadline,
          setDeadline,
        }}
        />
        <br/>
        <Typography.Title
          level={5}
        >
          Описание
        </Typography.Title>
        <Typography.Paragraph
          editable={{onChange: setDesc}}
        >
          {desc}
        </Typography.Paragraph>
      </div>
    </div>
  );
};
