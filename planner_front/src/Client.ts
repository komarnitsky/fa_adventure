import axios from 'axios';
import {apiHost, progresses} from './constants';
import {notification} from 'antd';
import moment, {Moment} from 'moment';

axios.interceptors.request.use(
    (config) => {
      if (
        config.url?.includes('/login/') ||
        config.url?.includes('/registration/')
      ) {
        delete config.headers!.authorization;
      } else {
        const token = localStorage.getItem('token');
        if (token) {
          config.headers!.authorization = `Token ${token}`;
        }
      }
      return config;
    },
    (error) => {
      return Promise.reject(error);
    },
);

const updateLocalCredsAndReload = (token: string, username: string) => {
  localStorage.setItem('token', token);
  localStorage.setItem('username', username);
  window.location.reload();
};

const createNotifications = (data: any) => {
  if (data === undefined || data === null) return;
  Object.entries(data).forEach(([key, value]) => {
    notification['error']({
      message: key,
      // @ts-ignore
      description: value[0],
    });
  });
};

export const login = (username: string, password: string) => {
  axios.post(
      `${apiHost}/api/login/`,
      {
        username,
        password,
      },
  ).then(({data}: any) => {
    updateLocalCredsAndReload(data.token, username);
  }).catch((error) => {
    createNotifications(error?.response?.data);
  });
};

export const register = (username: string, password: string, email: string) => {
  axios.post(
      `${apiHost}/api/registration/registration/`,
      {
        username,
        password,
        email,
      },
  ).then(({data}: any) => {
    updateLocalCredsAndReload(data.token, username);
  }).catch((error) => {
    createNotifications(error?.response?.data);
  });
};

export const getProjectsList = async () => axios.get(`${apiHost}/api/projects/projects`);

export const createNewProject = (
    setConfirmLoading: any,
    modalText: string,
    setModalVisible: any,
    setModalText: any,
) => {
  setConfirmLoading(true);
  axios.post(
      `${apiHost}/api/projects/projects/`,
      {
        name: modalText,
      },
  ).then(() => {
    setConfirmLoading(false);
    setModalVisible(false);
  }).catch((error) => {
    console.log(error);
    setModalText('');
    setModalVisible(false);
    setConfirmLoading(false);
  });
};

export const createTask = (
    newTitle: string,
    newPriority: 'low' | 'high' | 'critical',
    newDesc: string,
    newSkillPoints: number,
    newDeadline?: Moment,
) =>
  axios.post(
      `${apiHost}/api/projects/tasks/`,
      {
        name: newTitle,
        desc: newDesc,
        deadline: newDeadline?.toDate(),
        priority: newPriority,
        skill_points: newSkillPoints,
        status: 'new',
        project_id: window.location.hash.substr(2),
      },
  );

export const getTaskList = async (uuid: string) => axios.get(`${apiHost}/api/projects/project/${uuid}`);

export const updateTask = (
    uuid: string,
    title: string,
    progress: number,
    priority: 'high' | 'low' | 'critical',
    desc: string | undefined,
    deadline: moment.Moment,
) => {
  axios.put(
      `${apiHost}/api/projects/task/${uuid}/`,
      {
        title,
        status: progresses[progress],
        priority,
        desc,
        deadline,
      },
  );
};
