import {
  ClockCircleOutlined,
  CheckCircleOutlined,
  InfoCircleOutlined, UserOutlined, DesktopOutlined, BookOutlined,
} from '@ant-design/icons';
import React from 'react';

export const steps = ['Новая', 'В исполнении', 'Завершена'];

export const stepIcons = [
  <InfoCircleOutlined key="0" className="progress-icon" />,
  <ClockCircleOutlined key="1" className="progress-icon" />,
  <CheckCircleOutlined key="2" className="progress-icon" />,
];

export const apiHost = 'http://3.144.74.215';

export const projectIcons = {
  'Личное': <UserOutlined />,
  'Работа': <DesktopOutlined />,
  'Учеба': <BookOutlined />,
};

export const progresses = [
  'new',
  'inprogress',
  'done',
];

export const priorityHumane = {
  low: 'Низкий',
  high: 'Высокий',
  critical: 'Немедленный',
};
