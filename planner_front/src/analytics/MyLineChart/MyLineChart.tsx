import React, {FC} from 'react';
import EChartsReact from 'echarts-for-react';

export const MyLineChart: FC<{filters: any}> = ({filters}) => {
  let base = +new Date(filters.year, 0, 0);
  const oneDay = 24 * 3600 * 1000;
  const data = [[base, Math.random() * 50]];
  for (let i = 1; i < 365; i++) {
    base += oneDay;
    const now = new Date(base);
    data.push([+now, Math.max(Math.round((Math.random() - 0.5) * 20 + data[i - 1][1]), 0)]);
  }

  const option = {
    tooltip: {
      trigger: 'axis',
      position: function(pt: any[]) {
        return [pt[0], '10%'];
      },
    },
    title: {
      left: 'center',
      text: 'Динамика очков',
    },
    toolbox: {
      feature: {
        dataZoom: {
          yAxisIndex: 'none',
        },
        restore: {},
        saveAsImage: {},
      },
    },
    xAxis: {
      type: 'time',
      boundaryGap: false,
    },
    yAxis: {
      type: 'value',
      boundaryGap: [0, '100%'],
    },
    dataZoom: [
      {
        type: 'inside',
        start: 0,
        end: 20,
      },
      {
        start: 0,
        end: 20,
      },
    ],
    series: [
      {
        name: 'Fake Data',
        type: 'line',
        smooth: true,
        symbol: 'none',
        areaStyle: {},
        data: data,
      },
    ],
  };

  return (
    <EChartsReact option={option} style={{height: '300px'}} />
  );
};
