interface HeatmapRecord {
  timestamp: number;
  skillPoints: number;
}

export interface FilterConfiguration {
  year: number;
  project: 'personal' | 'work' | 'study' | 'any' | string;
}

export interface ChartContainerProps {
  chartType: 'heatmap' | 'line';
}

export interface HeatmapCalendarProps {
  filters: FilterConfiguration;
}

export interface HeatmapFilterProps {
  filters: FilterConfiguration;
  setFilters: (value: FilterConfiguration) => void;
}
