import React, {FC, useEffect} from 'react';
import EChartsReact from 'echarts-for-react';
import * as echarts from 'echarts';
import {HeatmapCalendarProps} from '../types';

// eslint-disable-next-line require-jsdoc
function getVirtulData(year: string) {
  year = year || '2017';
  const date = +echarts.number.parseDate(`${year}-01-01`);
  const end = +echarts.number.parseDate(`${+year + 1}-01-01`);
  const dayTime = 3600 * 24 * 1000;
  const data = [];
  for (let time = date; time < end; time += dayTime) {
    data.push([
      echarts.format.formatTime('yyyy-MM-dd', time),
      Math.floor(Math.random() * 10000),
    ]);
  }
  return data;
}

export const HeatmapCalendar: FC<HeatmapCalendarProps> = ({filters}) => {
  const options = {
    tooltip: {},
    visualMap: {
      min: 0,
      max: 10000,
      type: 'piecewise',
      orient: 'horizontal',
      left: 'center',
      inRange: {
        color: ['#bbf3bc', '#127214'],
        // color: ['#f6c771', '#a2202c'],
      },
    },
    calendar: {
      cellSize: ['auto', 13],
      range: filters.year,
      left: 30,
      dayLabel: {
        firstDay: 1,
        // nameMap: ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'],
        nameMap: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
      },
      monthLabel: {
        nameMap: [
          'Янв',
          'Фев',
          'Мар',
          'Апр',
          'Май',
          'Июнь',
          'Июль',
          'Авг',
          'Сен',
          'Окт',
          'Ноя',
          'Дек',
        ],
      },
      itemStyle: {
        borderWidth: 0.5,
      },
    },
    series: {
      type: 'heatmap',
      coordinateSystem: 'calendar',
      data: getVirtulData(filters.year.toString()),
    },
  };

  useEffect(() => {
    // TODO: fetch data with filters
    console.log(filters);
  }, [filters]);

  return (
    <EChartsReact option={options} style={{height: '200px'}} />
  );
};
