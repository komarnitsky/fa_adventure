import {
  Card, Col, Divider, Row,
} from 'antd';
import React, {FC, useState} from 'react';
import {HeatmapCalendar} from '../HeatmapCalendar/HeatmapCalendar';
import {ChartContainerProps} from '../types';
import {HeatmapFilter} from '../HeatmapCalendar/HeatmapFilter';
import './ChartContainer.css';
import {MyLineChart} from '../MyLineChart/MyLineChart';

export const ChartContainer: FC<ChartContainerProps> = ({chartType}) => {
  const [filters, setFilters] = useState({
    project: 'any',
    year: new Date().getFullYear(),
  });

  if (chartType === 'heatmap') {
    return (
      <div style={{display: 'flex', flexDirection: 'column'}}>
        <Card>
          <Row>
            <Col span={15}>
              <HeatmapCalendar filters={filters}/>
            </Col>
            <Col span={1}>
              <Divider
                type="vertical"
                style={{marginLeft: '20px', height: '100%'}}
              />
            </Col>
            <Col span={8}>
              <HeatmapFilter setFilters={setFilters} filters={filters}/>
            </Col>
          </Row>
        </Card>
        <Card>
          <MyLineChart filters={filters} />
        </Card>
      </div>

    );
  }
  return null;
};
