import React, {useState} from 'react';
import {Button, Form, Input} from 'antd';
import * as Client from './Client';
import {LockOutlined, UserOutlined} from '@ant-design/icons';

export const AuthPage = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [register, setRegister] = useState(false);

  const onFinish = () => {
    if (!register) {
      Client.login(username, password);
    } else {
      Client.register(username, password, email);
    }
  };

  return (
    <div className="login-container">
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{remember: true}}
        onFinish={onFinish}
      >
        <Form.Item
          name="username"
          rules={[{required: true, message: 'Введите свой логин!'}]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon"/>}
            placeholder="Логин"
            onChange={(e) => setUsername(e.target.value)}
          />
        </Form.Item>

        {register &&
        <Form.Item
          name="email"
          rules={[
            {
              type: 'email',
              message: 'Email не валидный!',
            },
            {
              required: true,
              message: 'Введите Email!',
            },
          ]}
        >
          <Input
            onChange={(e) => setEmail(e.target.value)}
            placeholder="Email"
          />
        </Form.Item>
        }

        <Form.Item
          name="password"
          rules={[{required: true, message: 'Введите свой пароль!'}]}
          hasFeedback
        >
          <Input.Password
            prefix={<LockOutlined className="site-form-item-icon"/>}
            type="password"
            placeholder="Пароль"
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Item>

        {register &&
        <Form.Item
          name="confirm"
          dependencies={['password']}
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Повторно введите пароль!',
            },
            ({getFieldValue}) => ({
              validator(_, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error('Пароли не совпадают'));
              },
            }),
          ]}>
          <Input.Password
            prefix={<LockOutlined className="site-form-item-icon"/>}
            type="password"
            placeholder="Пароль еще раз"
          />
        </Form.Item>
        }


        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            {!register && 'Войти'}
            {register && 'Зарегистрироваться'}
          </Button>
        Или
          <a onClick={() => setRegister(!register)}>
            {!register && ' зарегистрироваться!'}
            {register && ' войти!'}
          </a>
        </Form.Item>
      </Form>
    </div>
  );
};
