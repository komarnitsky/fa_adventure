import React from 'react';
import './App.css';
import {HashRouter, Route, Switch} from 'react-router-dom';
import {ProfilePage} from './profile/profilePage';
import {CustomProjectPage} from './profile/Content/CustomProjectPage';
import {AuthPage} from './AuthPage';

const App = () => (
  <HashRouter>
    <Switch>
      <Route exact path="/">
        {localStorage.getItem('token') && <ProfilePage activeTab="analytics" />}
        {!localStorage.getItem('token') && <AuthPage />}
      </Route>
      <Route path="/analytics">
        <ProfilePage activeTab="analytics" />
      </Route>
      <Route path="/work">
        <ProfilePage activeTab="work" />
      </Route>
      <Route path="/study">
        <ProfilePage activeTab="study" />
      </Route>
      <Route path="/:project">
        <CustomProjectPage />
      </Route>
    </Switch>
  </HashRouter>
);

export default App;
