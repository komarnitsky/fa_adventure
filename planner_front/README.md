[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=komarnitsky_fa_adventure&metric=bugs)](https://sonarcloud.io/summary/new_code?id=komarnitsky_fa_adventure)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=komarnitsky_fa_adventure&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=komarnitsky_fa_adventure)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=komarnitsky_fa_adventure&metric=coverage)](https://sonarcloud.io/summary/new_code?id=komarnitsky_fa_adventure)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=komarnitsky_fa_adventure&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=komarnitsky_fa_adventure)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=komarnitsky_fa_adventure&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=komarnitsky_fa_adventure)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=komarnitsky_fa_adventure&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=komarnitsky_fa_adventure)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=komarnitsky_fa_adventure&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=komarnitsky_fa_adventure)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=komarnitsky_fa_adventure&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=komarnitsky_fa_adventure)

Добавь этот код в .git/hooks/pre-commit. Теперь перед тем как закомитить изменения, скрипт будет проверять только измененные файлы на нарушения стилистики кода. И если таковые имеются, то операция будет прервана.
Все настройки качества кода находятся в файле *.jshintrc*




    #!/bin/bash
    
    filenames=($(git diff --cached --name-only HEAD))
    
    which jshint &> /dev/null
    if [ $? -ne 0 ];
    then
      echo "error: jshint not found"
      echo "install with: sudo npm install -g jshint"
      exit 1
    fi
    
    for i in "${filenames[@]}"
    do
        if [[ $i =~ \.js$ ]];
        then
            echo jshint $i
            jshint $i
            if [ $? -ne 0 ];
            then
                exit 1
            fi
        fi
    done

Чтобы закоммитить без проверки:

git commit --no-verify

Чтобы просто проверить код:

jshint имя_файла

jshint имя_папки/

Также jshint работает в IDE
