from projects import models

DEFAULT_PROJECTS = ["Личное", "Работа", "Учеба"]


def create_default_project(user):
    for name in DEFAULT_PROJECTS:
        models.Project.objects.create(name=name, owner=user)
