from django.urls import path, include
from registration import views

urlpatterns = [
    path("registration/", views.RegistrationView.as_view()),
]
