from rest_framework import serializers

from django.contrib.auth.models import User
from projects import models


def task_validate(self, attrs):
    if models.Task.objects.filter(
        name=attrs.get("name"), project=self.context["project"]
    ).exists():
        raise serializers.ValidationError("Задача с таким именем уже существует.")
    return attrs


def project_validate(self, attrs):
    if models.Project.objects.filter(
        name=attrs.get("name"), owner=self.context["request"].user
    ).exists():
        raise serializers.ValidationError("Проект с таким именем уже существует.")

    return attrs


class TaskListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Task
        fields = [
            "uuid",
            "name",
            "desc",
            "created_at",
            "deadline",
            "priority",
            "skill_points",
            "status",
            "project_id",
        ]

    validate = task_validate

    def create(self, validated_data):
        return models.Task.objects.create(
            **validated_data, project=self.context["project"]
        )


class TaskDetailSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=False)
    desc = serializers.CharField(required=False)
    deadline = serializers.DateTimeField(required=False)
    priority = serializers.CharField(required=False)
    skill_points = serializers.IntegerField(required=False)
    status = serializers.CharField(required=False)
    project_id = serializers.CharField(required=False)

    class Meta:
        model = models.Task
        fields = [
            "uuid",
            "name",
            "desc",
            "created_at",
            "deadline",
            "priority",
            "skill_points",
            "status",
            "project_id",
        ]

    validate = task_validate

    def update(self, instance: models.Task, validated_data):
        if validated_data.get("name") is not None:
            instance.name = validated_data.get("name")
        if validated_data.get("desc") is not None:
            instance.desc = validated_data.get("desc")
        if validated_data.get("deadline") is not None:
            instance.deadline = validated_data.get("deadline")
        if validated_data.get("priority") is not None:
            instance.priority = validated_data.get("priority")
        if validated_data.get("skill_points") is not None:
            instance.skill_poins = validated_data.get("skill_points")
        if validated_data.get("status") is not None:
            instance.status = validated_data.get("status")
        if validated_data.get("project_id") is not None:
            instance.project_id = validated_data.get("project_id")

        instance.save()
        return instance


class ProjectListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Project
        fields = ["created_at", "owner_id", "name", "uuid", "desc"]

    validate = project_validate

    def create(self, validated_data):
        return models.Project.objects.create(
            **validated_data, owner=self.context["request"].user
        )


class ProjectDetailSerializer(serializers.ModelSerializer):
    tasks = TaskListSerializer(many=True)

    class Meta:
        model = models.Project
        fields = ["created_at", "owner_id", "name", "uuid", "desc", "tasks"]

    validate = project_validate

    def update(self, instance: models.Project, validated_data):
        if validated_data.get("name") is not None:
            instance.name = validated_data.get("name")
        if validated_data.get("desc") is not None:
            instance.desc = validated_data.get("desc")

        instance.save()
        return instance
