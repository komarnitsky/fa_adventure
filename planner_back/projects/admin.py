from django.contrib import admin
from .models import Task, Project


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ["uuid", "owner", "name"]
    list_filter = ["owner"]


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ["uuid", "name", "project"]
    list_filter = ["project"]
