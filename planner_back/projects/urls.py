from django.urls import path, include
from projects import views

urlpatterns = [
    path("projects/", views.ProjectListAPIView.as_view()),
    path("project/<uuid:uuid>/", views.ProjectDetailAPIView.as_view()),
    path("task/<uuid:uuid>/", views.TaskDetailAPIView.as_view()),
    path("tasks/", views.TaskListAPIView.as_view()),
]
