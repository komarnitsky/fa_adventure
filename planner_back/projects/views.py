from django.contrib.auth.models import User
from django.http import Http404
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from projects import models, serializers

# from .permissions import OwnerOrReadOnly


class ProjectListAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        projects = models.Project.objects.filter(owner=request.user).order_by(
            "created_at"
        )
        serializer = serializers.ProjectListSerializer(
            projects, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = serializers.ProjectListSerializer(
            data=request.data, context={"request": request}
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)


class ProjectDetailAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, uuid):
        try:
            return models.Project.objects.get(uuid=uuid)
        except models.Project.DoesNotExist:
            raise Http404

    def get(self, request, uuid):
        project = self.get_object(uuid)
        serializer = serializers.ProjectDetailSerializer(project)
        return Response(serializer.data)

    def put(self, request, uuid):
        project = self.get_object(uuid)
        # self.check_object_permissions(request, project)
        serializer = serializers.ProjectDetailSerializer(
            project, data=request.data, context={"request": request}
        )

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data)

    def delete(self, request, uuid):
        project = self.get_object(uuid)
        project.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TaskListAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get_project(self, request):
        try:
            return models.Project.objects.get(uuid=request.data["project_id"])
        except:
            raise Http404

    def get(self, request):
        tasks = models.Task.objects.filter(project__owner=request.user).order_by("created_at")
        serializer = serializers.TaskListSerializer(
            tasks, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def post(self, request):
        project = self.get_project(request)
        serializer = serializers.TaskListSerializer(
            data=request.data, context={"request": request, "project": project}
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)


class TaskDetailAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, uuid):
        try:
            return models.Task.objects.get(uuid=uuid)
        except models.Task.DoesNotExist:
            raise Http404

    def get(self, request, uuid):
        task = self.get_object(uuid)
        serializer = serializers.TaskDetailSerializer(task)
        return Response(serializer.data)

    def put(self, request, uuid):
        task = self.get_object(uuid)
        # self.check_object_permissions(request, project)
        serializer = serializers.TaskDetailSerializer(
            task,
            data=request.data,
            context={"request": request, "project": task.project},
        )

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data)

    def delete(self, request, uuid):
        task = self.get_object(uuid)
        task.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
