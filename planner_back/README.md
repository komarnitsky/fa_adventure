Для использования статического анализатора pyflake, нужно установить flake8 и flake8-django.
В директории planner_back выполнить команду:
 *flake8 --select=E,F,W,C90,DJ,DJ10*
 